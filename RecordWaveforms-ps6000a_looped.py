import ctypes
from picosdk.ps6000a import ps6000a as ps
import numpy as np
from picosdk.functions import mV2adc, adc2mV, assert_pico_ok
from picosdk.PicoDeviceEnums import picoEnum as enums
from picosdk.PicoDeviceStructs import *
from datetime import datetime
import json
import time
import os
import sys

startTime = time.time()

# Define path for this run, create folder
#rootpath = 'waveforms/test/'
rootpath = '/media/ilcuser/T7/DESY_Timing_Study/test/'

# Create config dictionary and define all settings
config = {}

config['startTime'] = startTime
config['runDateTime'] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

config['loops'] = 6000
config['nWaveforms'] = 10000

config['resolution'] = enums.PICO_DEVICE_RESOLUTION["PICO_DR_8BIT"]

#10mV [0], 20mV [1], 50mV [2], 100mV [3], 200mV [4], 500mV [5]
config['coupling'] = enums.PICO_COUPLING["PICO_AC"] 
config['channelRange'] = 6 
config['analogueOffset'] = 0.950
config['bandwidth'] = enums.PICO_BANDWIDTH_LIMITER["PICO_BW_FULL"]

# Trigger description to be saved in the config file
# This is just free text so that we remember what we did
config['triggerDesc'] = 'Coincidence trigger on CHA + CHC (logic AND)'

# Trigger threshold and hysteresis
maxADC = ctypes.c_int16(32512)
config['maxADC'] = maxADC.value
config['threshold_pe'] = 20
config['threshold_mV'] = config['threshold_pe']*-13.5+config['analogueOffset']*1000
config['threshold'] = mV2adc(config['threshold_mV'], config['channelRange'], maxADC)
config['hysteresis'] = mV2adc(int(np.abs(config['threshold_mV']) * 0.015), config['channelRange'], maxADC)

# Additional trigger settings
config['auxOutputEnable'] = 0
config['autoTriggerMilliseconds'] = 0

# Number of samples per waveform
config['preTriggerSamples'] = 1000
config['postTriggerSamples'] = 2000

# Timebase (see manual)
config['timebase'] = 1

# Buffer settings (should not be changed in most applications)
config['PicoDataType'] = enums.PICO_DATA_TYPE["PICO_INT16_T"]
config['PicoRatioMode'] = enums.PICO_RATIO_MODE["PICO_RATIO_MODE_AGGREGATE"]

# Do not change these file path settings
runConfigFile = rootpath + 'runConfig.json'

if os.path.isfile(runConfigFile):
	with open(runConfigFile) as json_file: 
		runConfig = json.load(json_file)
	runConfig['lastRunNumber'] += 1
	runNumber = runConfig['lastRunNumber']
else:
	runConfig = {'lastRunNumber': 1}
	runNumber = 1
with open(runConfigFile, 'w') as fp:
	json.dump(runConfig, fp, indent=2)

runpath = rootpath + 'run{}/'.format(runNumber)
os.mkdir(runpath)
configpath = runpath + 'config.json'
datapath = runpath + 'Bulk{}-Ch{}-wf{}-{}.bin'

print("Initializing run number {} (target directoy: {})".format(runNumber, runpath))
config['runNumber'] = runNumber

# Create chandle
chandle = ctypes.c_int16()

# Opens the device/s
assert_pico_ok(ps.ps6000aOpenUnit(ctypes.byref(chandle), None, config['resolution']))

# Displays the serial number and handle
print("Connected to PicoScope, handle value = {}".format(chandle.value))

# Set channels on
# handle = chandle
channelA = enums.PICO_CHANNEL["PICO_CHANNEL_A"]
channelC = enums.PICO_CHANNEL["PICO_CHANNEL_C"]
channelE = enums.PICO_CHANNEL["PICO_CHANNEL_E"]
channelG = enums.PICO_CHANNEL["PICO_CHANNEL_G"]

assert_pico_ok(ps.ps6000aSetChannelOn(chandle, channelA, config['coupling'], config['channelRange'], config['analogueOffset'], config['bandwidth']))
assert_pico_ok(ps.ps6000aSetChannelOn(chandle, channelC, config['coupling'], config['channelRange'], config['analogueOffset'], config['bandwidth']))
assert_pico_ok(ps.ps6000aSetChannelOn(chandle, channelE, config['coupling'], config['channelRange'], config['analogueOffset'], config['bandwidth']))
assert_pico_ok(ps.ps6000aSetChannelOn(chandle, channelG, config['coupling'], config['channelRange'], config['analogueOffset'], config['bandwidth']))


# set channel B, D, F, H off
for x in [1, 3, 5, 7]:
	channel = x
	assert_pico_ok(ps.ps6000aSetChannelOff(chandle, channel))

# Set logic trigger --> COINCIDENCE of Channels A and C
# These conditions produce a logical AND
conditions_arr = (PICO_CONDITION * 2)()
conditions_arr[0] = PICO_CONDITION(source=channelA, condition=1)
conditions_arr[1] = PICO_CONDITION(source=channelC, condition=1)

assert_pico_ok(ps.ps6000aSetTriggerChannelConditions(chandle, 
	ctypes.byref(conditions_arr),
	2,
	enums.PICO_ACTION["PICO_ADD"]
))

directions_arr = (PICO_DIRECTION * 2)()
directions_arr[0] = PICO_DIRECTION(
	channelA,
	enums.PICO_THRESHOLD_DIRECTION["PICO_FALLING"],
	enums.PICO_THRESHOLD_MODE["PICO_LEVEL"]
)
directions_arr[1] = PICO_DIRECTION(
	channelC,
	enums.PICO_THRESHOLD_DIRECTION["PICO_FALLING"],
	enums.PICO_THRESHOLD_MODE["PICO_LEVEL"]
)
assert_pico_ok(ps.ps6000aSetTriggerChannelDirections(chandle, ctypes.byref(directions_arr), 2))

#Set Trigger Treshold in PE/mV/ADC
print("Threshold: {}".format(config['threshold']))

struct_arr = (PICO_TRIGGER_CHANNEL_PROPERTIES * 2)()
struct_arr[0] = PICO_TRIGGER_CHANNEL_PROPERTIES(
	config['threshold'],
	config['hysteresis'],
	config['threshold'],
	config['hysteresis'],
	channelA
)
struct_arr[1] = PICO_TRIGGER_CHANNEL_PROPERTIES(
	config['threshold'],
	config['hysteresis'],
	config['threshold'],
	config['hysteresis'],
	channelC
)

assert_pico_ok(ps.ps6000aSetTriggerChannelProperties(chandle,
	ctypes.byref(struct_arr),
	2,
	config['auxOutputEnable'],
	config['autoTriggerMilliseconds']
))

# Setting the number of sample to be collected
maxsamples = config['preTriggerSamples'] + config['postTriggerSamples']

# Gets sampling time (timebase) infomation
timeIntervalns = ctypes.c_double()
returnedMaxSamples = ctypes.c_int64()
assert_pico_ok(ps.ps6000aGetTimebase(chandle,
	config['timebase'],
	maxsamples,
	ctypes.byref(timeIntervalns),
	ctypes.byref(returnedMaxSamples),
	0
))
config['timeIntervalns'] = timeIntervalns.value
config['returnedMaxSamples'] = returnedMaxSamples.value
print("Time interval (ns) = {}, max samples = {}".format(timeIntervalns.value, returnedMaxSamples.value))
print(" --> max. {:.0f} waveforms possible | {:.0f} samples @ {} waveforms".format(returnedMaxSamples.value/maxsamples, returnedMaxSamples.value/config['nWaveforms'], config['nWaveforms']))

# Creates a overlow location for data
#overflow = ctypes.c_int16()
# Creates converted types maxsamples
cmaxMemorySegments = ctypes.c_int32(maxsamples)

assert_pico_ok(ps.ps6000aMemorySegments(chandle,
	config['nWaveforms'],
	ctypes.byref(cmaxMemorySegments)
))

# sets number of captures
assert_pico_ok(ps.ps6000aSetNoOfCaptures(chandle, config['nWaveforms']))

print("[{:.4f} s] Setting buffers".format(time.time() - startTime))

bufferAMax = []
bufferCMax = []
bufferEMax = []
bufferGMax = []

for wf in range(int(config['nWaveforms'])):
	# Create buffers ready for assigning pointers for data collection
	bufferAMax.append((ctypes.c_int16 * maxsamples)())
	bufferCMax.append((ctypes.c_int16 * maxsamples)())
	bufferEMax.append((ctypes.c_int16 * maxsamples)())
	bufferGMax.append((ctypes.c_int16 * maxsamples)())

	assert_pico_ok(ps.ps6000aSetDataBuffer(chandle,
		channelA,
		ctypes.byref(bufferAMax[wf]),
		maxsamples,
		config['PicoDataType'],
		wf,
		config['PicoRatioMode'],
		enums.PICO_ACTION["PICO_ADD"]
	))
	assert_pico_ok(ps.ps6000aSetDataBuffer(chandle,
		channelC,
		ctypes.byref(bufferCMax[wf]),
		maxsamples,
		config['PicoDataType'],
		wf,
		config['PicoRatioMode'],
		enums.PICO_ACTION["PICO_ADD"]
	))
	assert_pico_ok(ps.ps6000aSetDataBuffer(chandle,
		channelE,
		ctypes.byref(bufferEMax[wf]),
		maxsamples,
		config['PicoDataType'],
		wf,
		config['PicoRatioMode'],
		enums.PICO_ACTION["PICO_ADD"]
	))
	assert_pico_ok(ps.ps6000aSetDataBuffer(chandle,
		channelG,
		ctypes.byref(bufferGMax[wf]),
		maxsamples,
		config['PicoDataType'],
		wf,
		config['PicoRatioMode'],
		enums.PICO_ACTION["PICO_ADD"]
	))


# Save config
with open(configpath, 'w') as fp:
	json.dump(config, fp, indent=2)

# Start block capture loops
print("[{:.4f} s] Starting block capture loops".format(time.time()-startTime))

# Checks data collection to finish the capture
ready = ctypes.c_int16(0)
check = ctypes.c_int16(0)

# Creates a overlow location for data
overflow = (ctypes.c_int16 * 10)()
# Creates converted types maxsamples
cmaxSamples = ctypes.c_int32(maxsamples)

for n in range(config['loops']):
	print("Loop Number: {}".format(n))
	# Starts the block capture
	timeIndisposedMs = ctypes.c_double()
	assert_pico_ok(ps.ps6000aRunBlock(chandle,
		config['preTriggerSamples'],
		config['postTriggerSamples'],
		config['timebase'],
		ctypes.byref(timeIndisposedMs),
		0,
		None,
		None
	))


	runStartTime = time.time()
	print("[{:.4f} s] Waiting for scope to be ready".format(runStartTime - startTime))

	ready.value = 0
	check.value = 0

	#Data collection & timeout setting
	while ready.value == check.value: #and time.time() < runStartTime + timeout:
		ps.ps6000aIsReady(chandle, ctypes.byref(ready))
	   # if time.time() > runStartTime + timeout:
	   #     print("Trigger Timeout!")
	   #     break

	readOutTime = time.time()
	print(" --> Recorded {} waveforms with {} samples in {} s".format(config['nWaveforms'], maxsamples, readOutTime-runStartTime))

	# Read out blocks in bulks
	cmaxSamples.value = int(maxsamples)
	bulkSize = int(config['nWaveforms'])
	segFrom = 0
	segTo = bulkSize - 1
	assert_pico_ok(ps.ps6000aGetValuesBulk(chandle,
		0,
		ctypes.byref(cmaxSamples),
		segFrom,
		segTo,
		1,
		config['PicoRatioMode'],
		ctypes.byref(overflow)
	))

	print("[{:.4f} s] Bulk values read out ({} -> {})".format(time.time() - startTime, segFrom, segTo))

	print("Data readout time: {}".format(time.time() - readOutTime))

	# Save data
	with open(datapath.format(n, "A", segFrom, segTo), "wb") as file:
		for i in range(config['nWaveforms']):
			file.write(bufferAMax[i])

	with open(datapath.format(n, "C", segFrom, segTo), "wb") as file:
		for i in range(config['nWaveforms']):
			file.write(bufferCMax[i])

	with open(datapath.format(n, "E", segFrom, segTo), "wb") as file:
		for i in range(config['nWaveforms']):
			file.write(bufferEMax[i])

	with open(datapath.format(n, "G", segFrom, segTo), "wb") as file:
		for i in range(config['nWaveforms']):
			file.write(bufferGMax[i])
						
	print("Data save time: {}".format(time.time() - readOutTime))


	print("[{:.4f} s] Dataframes saved".format(time.time() - startTime))

	print("Readout and save time: {}".format(time.time() - readOutTime))



#print(time.time()-start)
# Closes the unit
# Handle = chandle
assert_pico_ok(ps.ps6000aCloseUnit(chandle))
print("pico closed")
sys.exit()