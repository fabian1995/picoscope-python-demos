import ctypes
from picosdk.ps6000a import ps6000a as ps
import numpy as np
from picosdk.functions import mV2adc, adc2mV, assert_pico_ok
from picosdk.PicoDeviceEnums import picoEnum as enums
from picosdk.PicoDeviceStructs import *

import time
import pandas as pd
start=time.time()
# Number of waveforms to be recorded
nWaveforms = 10000
nBulks = 1

filepath = "waveforms/Run1-Ch{}-wf{}-{}.bin"

# Create chandle
chandle = ctypes.c_int16()
status = {}

# Opens the device/s
resolution = enums.PICO_DEVICE_RESOLUTION["PICO_DR_8BIT"]
status["openunit"] = ps.ps6000aOpenUnit(ctypes.byref(chandle), None, resolution)
assert_pico_ok(status["openunit"])

# Displays the serial number and handle
print("Connected to PicoScope, handle value = {}".format(chandle.value))

# Set channels on
# handle = chandle
channelA = enums.PICO_CHANNEL["PICO_CHANNEL_A"]
channelC = enums.PICO_CHANNEL["PICO_CHANNEL_C"]
channelE = enums.PICO_CHANNEL["PICO_CHANNEL_E"]
channelG = enums.PICO_CHANNEL["PICO_CHANNEL_G"]
coupling = enums.PICO_COUPLING["PICO_DC_50OHM"]

#10mV [0], 20mV [1], 50mV [2], 100mV [3], 200mV [4], 500mV [5] 
channelRange = 6 
analogueOffset = 0
bandwidth = enums.PICO_BANDWIDTH_LIMITER["PICO_BW_FULL"]
assert_pico_ok(ps.ps6000aSetChannelOn(chandle, channelA, coupling, channelRange, analogueOffset, bandwidth))
assert_pico_ok(ps.ps6000aSetChannelOn(chandle, channelC, coupling, channelRange, analogueOffset, bandwidth))
assert_pico_ok(ps.ps6000aSetChannelOn(chandle, channelE, coupling, channelRange, analogueOffset, bandwidth))
assert_pico_ok(ps.ps6000aSetChannelOn(chandle, channelG, coupling, channelRange, analogueOffset, bandwidth))


# set channel B, D, F, H off
for x in [1, 3, 5, 7]:
    channel = x
    assert_pico_ok(ps.ps6000aSetChannelOff(chandle, channel))

# Set logic trigger --> COINCIDENCE of Channels A and C
# These conditions produce a logical AND
conditions_arr = (PICO_CONDITION * 2)()
conditions_arr[0] = PICO_CONDITION(source=channelA, condition=1)
conditions_arr[1] = PICO_CONDITION(source=channelC, condition=1)

assert_pico_ok(ps.ps6000aSetTriggerChannelConditions(chandle, 
	ctypes.byref(conditions_arr),
	2,
	enums.PICO_ACTION["PICO_ADD"]
))

directions_arr = (PICO_DIRECTION * 2)()
directions_arr[0] = PICO_DIRECTION(
	channelA,
	enums.PICO_THRESHOLD_DIRECTION["PICO_FALLING"],
	enums.PICO_THRESHOLD_MODE["PICO_LEVEL"]
)
directions_arr[1] = PICO_DIRECTION(
	channelC,
	enums.PICO_THRESHOLD_DIRECTION["PICO_FALLING"],
	enums.PICO_THRESHOLD_MODE["PICO_LEVEL"]
)
assert_pico_ok(ps.ps6000aSetTriggerChannelDirections(chandle, ctypes.byref(directions_arr), 2))

#Set Trigger Treshold in PE/mV/ADC
maxADC = ctypes.c_int16(32512)
threshold_pe = 20
threshold_mV = threshold_pe*-13.5
threshold = mV2adc(threshold_mV, channelRange, maxADC)
hysteresis = mV2adc(int(np.abs(threshold_mV) * 0.015), channelRange, maxADC)
print("Threshold: {}".format(threshold))

struct_arr = (PICO_TRIGGER_CHANNEL_PROPERTIES * 2)()
struct_arr[0] = PICO_TRIGGER_CHANNEL_PROPERTIES(
	threshold,
	hysteresis,
	threshold,
	hysteresis,
	channelA
)
struct_arr[1] = PICO_TRIGGER_CHANNEL_PROPERTIES(
	threshold,
	hysteresis,
	threshold,
	hysteresis,
	channelC
)
auxOutputEnable = 0
autoTriggerMilliseconds = 0
assert_pico_ok(ps.ps6000aSetTriggerChannelProperties(chandle,
	ctypes.byref(struct_arr),
	2,
	auxOutputEnable,
	autoTriggerMilliseconds
))

# Setting the number of sample to be collected
preTriggerSamples = 1000
postTriggerSamples = 2000
maxsamples = preTriggerSamples + postTriggerSamples

# Gets sampling time (timebase) infomation
timebase = 1
timeIntervalns = ctypes.c_double()
returnedMaxSamples = ctypes.c_int64()
assert_pico_ok(ps.ps6000aGetTimebase(chandle,
	timebase,
	maxsamples,
	ctypes.byref(timeIntervalns),
	ctypes.byref(returnedMaxSamples),
	0
))
print("Time interval (ns) = {}, max samples = {}".format(timeIntervalns.value, returnedMaxSamples.value))
print(" --> max. {:.0f} waveforms possible | {:.0f} samples @ {} waveforms".format(returnedMaxSamples.value/maxsamples, returnedMaxSamples.value/nWaveforms, nWaveforms))

# Creates a overlow location for data
#overflow = ctypes.c_int16()
# Creates converted types maxsamples
cmaxSegments = ctypes.c_int32(maxsamples)

assert_pico_ok(ps.ps6000aMemorySegments(chandle,
	nWaveforms,
	ctypes.byref(cmaxSegments)
))

# sets number of captures
assert_pico_ok(ps.ps6000aSetNoOfCaptures(chandle, nWaveforms))

startTime = time.time()
print("[{:.4f} s] Setting buffers".format(time.time() - startTime))

bufferAMax = []
bufferCMax = []
bufferEMax = []
bufferGMax = []


bulkSize = int(nWaveforms/nBulks)
for wf in range(bulkSize):
	# Create buffers ready for assigning pointers for data collection
	bufferAMax.append((ctypes.c_int16 * maxsamples)())
	bufferCMax.append((ctypes.c_int16 * maxsamples)())
	bufferEMax.append((ctypes.c_int16 * maxsamples)())
	bufferGMax.append((ctypes.c_int16 * maxsamples)())
	

	for bulk in range(nBulks):
		assert_pico_ok(ps.ps6000aSetDataBuffer(chandle,
			channelA,
			ctypes.byref(bufferAMax[wf]),
			maxsamples,
			enums.PICO_DATA_TYPE["PICO_INT16_T"],
			wf+bulkSize*bulk,
			enums.PICO_RATIO_MODE["PICO_RATIO_MODE_AGGREGATE"],
			enums.PICO_ACTION["PICO_ADD"]
		))
		assert_pico_ok(ps.ps6000aSetDataBuffer(chandle,
			channelC,
			ctypes.byref(bufferCMax[wf]),
			maxsamples,
			enums.PICO_DATA_TYPE["PICO_INT16_T"],
			wf+bulkSize*bulk,
			enums.PICO_RATIO_MODE["PICO_RATIO_MODE_AGGREGATE"],
			enums.PICO_ACTION["PICO_ADD"]
		))
		assert_pico_ok(ps.ps6000aSetDataBuffer(chandle,
			channelE,
			ctypes.byref(bufferEMax[wf]),
			maxsamples,
			enums.PICO_DATA_TYPE["PICO_INT16_T"],
			wf+bulkSize*bulk,
			enums.PICO_RATIO_MODE["PICO_RATIO_MODE_AGGREGATE"],
			enums.PICO_ACTION["PICO_ADD"]
		))
		assert_pico_ok(ps.ps6000aSetDataBuffer(chandle,
			channelG,
			ctypes.byref(bufferGMax[wf]),
			maxsamples,
			enums.PICO_DATA_TYPE["PICO_INT16_T"],
			wf+bulkSize*bulk,
			enums.PICO_RATIO_MODE["PICO_RATIO_MODE_AGGREGATE"],
			enums.PICO_ACTION["PICO_ADD"]
		))

print("[{:.4f} s] Starting block capture".format(time.time()-startTime))

# Starts the block capture
timeIndisposedMs = ctypes.c_double()
assert_pico_ok(ps.ps6000aRunBlock(chandle,
	preTriggerSamples,
	postTriggerSamples,
	timebase,
	ctypes.byref(timeIndisposedMs),
	0,
	None,
	None
))

# Checks data collection to finish the capture
ready = ctypes.c_int16(0)
check = ctypes.c_int16(0)

runStartTime = time.time()
print("[{:.4f} s] Waiting for scope to be ready".format(runStartTime - startTime))


#Data collection & timeout setting
timeout=300 #[s]

while ready.value == check.value: #and time.time() < runStartTime + timeout:
    ps.ps6000aIsReady(chandle, ctypes.byref(ready))
   # if time.time() > runStartTime + timeout:
   #     print("Trigger Timeout!")
   #     break

readOutTime = time.time()
print(" --> Recorded {} waveforms with {} samples in {} s".format(nWaveforms, maxsamples, readOutTime-runStartTime))

# Creates a overlow location for data
overflow = (ctypes.c_int16 * 10)()
# Creates converted types maxsamples
cmaxSamples = ctypes.c_int32(maxsamples)

# Read out blocks in bulks
bulkSize = int(nWaveforms/nBulks)
for bulk in range(nBulks):
	segFrom = bulkSize * bulk
	segTo = bulkSize * (bulk+1) - 1
	assert_pico_ok(ps.ps6000aGetValuesBulk(chandle,
		0,
		ctypes.byref(cmaxSamples),
		segFrom,
		segTo,
		1,
		enums.PICO_RATIO_MODE["PICO_RATIO_MODE_AGGREGATE"],
		ctypes.byref(overflow)
	))

	print("[{:.4f} s] Bulk values read out ({} -> {})".format(time.time() - startTime, segFrom, segTo))

	print("Data readout time: {}".format(time.time() - readOutTime))

	# Save data
	with open(filepath.format("A", segFrom, segTo), "wb") as file:
		for i in range(nWaveforms):
			file.write(bufferAMax[i])

	with open(filepath.format("C", segFrom, segTo), "wb") as file:
		for i in range(nWaveforms):
			file.write(bufferCMax[i])

	with open(filepath.format("E", segFrom, segTo), "wb") as file:
		for i in range(nWaveforms):
			file.write(bufferEMax[i])

	with open(filepath.format("G", segFrom, segTo), "wb") as file:
		for i in range(nWaveforms):
			file.write(bufferGMax[i])
					
	print("Data save time: {}".format(time.time() - readOutTime))


print("[{:.4f} s] Dataframes saved".format(time.time() - startTime))

print("Readout and save time: {}".format(time.time() - readOutTime))
#print(time.time()-start)
# Closes the unit
# Handle = chandle
print("closing picoscope")
status["closeunit"] = ps.ps6000aCloseUnit(chandle)
assert_pico_ok(status["closeunit"])
print("picoscope closed")
