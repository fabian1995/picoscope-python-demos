import ctypes
from picosdk.ps6000a import ps6000a as ps
import numpy as np
from picosdk.functions import mV2adc, adc2mV, assert_pico_ok
from picosdk.PicoDeviceEnums import picoEnum as enums
from picosdk.PicoDeviceStructs import *
from datetime import datetime
import json
import time
import os
import sys

startTime = time.time()

# Define path for this run, create folder
rootpath = 'waveforms/'
rootpath = '/media/ilcuser/T7/DESY_Timing_Study_Test/1pe_Calibrations/'


# Create config dictionary and define all settings
config = {}

config['startTime'] = startTime
config['runDateTime'] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

config['nWaveforms'] = 2000

config['resolution'] = enums.PICO_DEVICE_RESOLUTION["PICO_DR_8BIT"]

#10mV [0], 20mV [1], 50mV [2], 100mV [3], 200mV [4], 500mV [5]
config['coupling'] = enums.PICO_COUPLING["PICO_AC"] 
config['channelRange'] = 2
config['analogueOffset'] = 0
config['bandwidth'] = enums.PICO_BANDWIDTH_LIMITER["PICO_BW_FULL"]

# Trigger description to be saved in the config file
# This is just free text so that we remember what we did
config['triggerDesc'] = 'Coincidence trigger on CHA + CHC (logic AND)'

# Trigger threshold and hysteresis
maxADC = ctypes.c_int16(32512)
config['maxADC'] = maxADC.value
config['threshold_mV'] = -8
config['threshold'] = mV2adc(config['threshold_mV'], config['channelRange'], maxADC)
config['hysteresis'] = mV2adc(int(np.abs(config['threshold_mV']) * 0.015), config['channelRange'], maxADC)

# Additional trigger settings
config['auxOutputEnable'] = 0
config['autoTriggerMilliseconds'] = 0

# Number of samples per waveform
config['preTriggerSamples'] = 500
config['postTriggerSamples'] = 1000

# Timebase (see manual)
config['timebase'] = 1

# Buffer settings (should not be changed in most applications)
config['PicoDataType'] = enums.PICO_DATA_TYPE["PICO_INT16_T"]
config['PicoRatioMode'] = enums.PICO_RATIO_MODE["PICO_RATIO_MODE_AGGREGATE"]

# Do not change these file path settings
runConfigFile = rootpath + 'runConfig.json'

if os.path.isfile(runConfigFile):
	with open(runConfigFile) as json_file: 
		runConfig = json.load(json_file)
	runConfig['lastRunNumber'] += 1
	runNumber = runConfig['lastRunNumber']
else:
	runConfig = {'lastRunNumber': 1}
	runNumber = 1
with open(runConfigFile, 'w') as fp:
	json.dump(runConfig, fp, indent=2)

runpath = rootpath + 'run{}-1peCal/'.format(runNumber)
os.mkdir(runpath)
configpath = runpath + 'config.json'
datapath = runpath + 'Bulk{}-Ch{}-wf{}-{}.bin'

print("Initializing run number {} (target directoy: {})".format(runNumber, runpath))
config['runNumber'] = runNumber

# Set channels on
# handle = chandle
channelA = enums.PICO_CHANNEL["PICO_CHANNEL_A"]
channelC = enums.PICO_CHANNEL["PICO_CHANNEL_C"]
channelE = enums.PICO_CHANNEL["PICO_CHANNEL_E"]
channelG = enums.PICO_CHANNEL["PICO_CHANNEL_G"]

triggerChannels = [channelA, channelC, channelE, channelG]
triggerChannelNames = ["A", "C", "E", "G"]


for iteration, trigName, trigCH in zip(range(len(triggerChannels)), triggerChannelNames, triggerChannels):
	print("Initializing Channel {}".format(trigName))
	# Create chandle
	if iteration == 0: chandle = ctypes.c_int16()

	# Opens the device/s
	assert_pico_ok(ps.ps6000aOpenUnit(ctypes.byref(chandle), None, config['resolution']))

	# Displays the serial number and handle
	print("Connected to PicoScope, handle value = {}".format(chandle.value))

	# set channel B, D, F, H off
	for x in range(8):
		channel = x
		assert_pico_ok(ps.ps6000aSetChannelOff(chandle, channel))

	assert_pico_ok(ps.ps6000aSetChannelOn(chandle, trigCH, config['coupling'], config['channelRange'], config['analogueOffset'], config['bandwidth']))

	# Set logic trigger --> COINCIDENCE of Channels A and C
	# These conditions produce a logical AND
	conditions_arr = (PICO_CONDITION * 1)()
	conditions_arr[0] = PICO_CONDITION(source=trigCH, condition=1)

	assert_pico_ok(ps.ps6000aSetTriggerChannelConditions(chandle, 
		ctypes.byref(conditions_arr),
		1,
		enums.PICO_ACTION["PICO_ADD"]
	))

	directions_arr = (PICO_DIRECTION * 1)()
	directions_arr[0] = PICO_DIRECTION(
		trigCH,
		enums.PICO_THRESHOLD_DIRECTION["PICO_FALLING"],
		enums.PICO_THRESHOLD_MODE["PICO_LEVEL"]
	)
	assert_pico_ok(ps.ps6000aSetTriggerChannelDirections(chandle, ctypes.byref(directions_arr), 1))

	#Set Trigger Treshold in PE/mV/ADC
	print("Threshold: {}".format(config['threshold']))

	struct_arr = (PICO_TRIGGER_CHANNEL_PROPERTIES * 1)()
	struct_arr[0] = PICO_TRIGGER_CHANNEL_PROPERTIES(
		config['threshold'],
		config['hysteresis'],
		config['threshold'],
		config['hysteresis'],
		trigCH
	)

	assert_pico_ok(ps.ps6000aSetTriggerChannelProperties(chandle,
		ctypes.byref(struct_arr),
		1,
		config['auxOutputEnable'],
		config['autoTriggerMilliseconds']
	))

	# Setting the number of sample to be collected
	maxsamples = config['preTriggerSamples'] + config['postTriggerSamples']

	# Gets sampling time (timebase) infomation
	timeIntervalns = ctypes.c_double()
	returnedMaxSamples = ctypes.c_int64()
	assert_pico_ok(ps.ps6000aGetTimebase(chandle,
		config['timebase'],
		maxsamples,
		ctypes.byref(timeIntervalns),
		ctypes.byref(returnedMaxSamples),
		0
	))
	config['timeIntervalns'] = timeIntervalns.value
	config['returnedMaxSamples'] = returnedMaxSamples.value
	print("Time interval (ns) = {}, max samples = {}".format(timeIntervalns.value, returnedMaxSamples.value))
	print(" --> max. {:.0f} waveforms possible | {:.0f} samples @ {} waveforms".format(returnedMaxSamples.value/maxsamples, returnedMaxSamples.value/config['nWaveforms'], config['nWaveforms']))

	# Creates a overlow location for data
	#overflow = ctypes.c_int16()
	# Creates converted types maxsamples
	cmaxMemorySegments = ctypes.c_int32(maxsamples)

	assert_pico_ok(ps.ps6000aMemorySegments(chandle,
		config['nWaveforms'],
		ctypes.byref(cmaxMemorySegments)
	))

	# sets number of captures
	assert_pico_ok(ps.ps6000aSetNoOfCaptures(chandle, config['nWaveforms']))

	print("[{:.4f} s] Setting buffers".format(time.time() - startTime))

	bufferAMax = []

	for wf in range(int(config['nWaveforms'])):
		# Create buffers ready for assigning pointers for data collection
		bufferAMax.append((ctypes.c_int16 * maxsamples)())
		
		assert_pico_ok(ps.ps6000aSetDataBuffer(chandle,
			trigCH,
			ctypes.byref(bufferAMax[wf]),
			maxsamples,
			config['PicoDataType'],
			wf,
			config['PicoRatioMode'],
			enums.PICO_ACTION["PICO_ADD"]
		))


	# Save config
	with open(configpath, 'w') as fp:
		json.dump(config, fp, indent=2)

	# Start block capture loops
	print("[{:.4f} s] Starting block capture loops".format(time.time()-startTime))

	# Checks data collection to finish the capture
	ready = ctypes.c_int16(0)
	check = ctypes.c_int16(0)

	# Creates a overlow location for data
	overflow = (ctypes.c_int16 * 10)()
	# Creates converted types maxsamples
	cmaxSamples = ctypes.c_int32(maxsamples)

	# Starts the block capture
	timeIndisposedMs = ctypes.c_double()
	assert_pico_ok(ps.ps6000aRunBlock(chandle,
		config['preTriggerSamples'],
		config['postTriggerSamples'],
		config['timebase'],
		ctypes.byref(timeIndisposedMs),
		0,
		None,
		None
	))


	runStartTime = time.time()
	print("[{:.4f} s] Waiting for scope to be ready".format(runStartTime - startTime))

	ready.value = 0
	check.value = 0

	#Data collection & timeout setting
	while ready.value == check.value: #and time.time() < runStartTime + timeout:
		ps.ps6000aIsReady(chandle, ctypes.byref(ready))
	   # if time.time() > runStartTime + timeout:
	   #     print("Trigger Timeout!")
	   #     break

	readOutTime = time.time()
	print(" --> Recorded {} waveforms with {} samples in {} s".format(config['nWaveforms'], maxsamples, readOutTime-runStartTime))

	# Read out blocks in bulks
	cmaxSamples.value = int(maxsamples)
	bulkSize = int(config['nWaveforms'])
	segFrom = 0
	segTo = bulkSize - 1
	assert_pico_ok(ps.ps6000aGetValuesBulk(chandle,
		0,
		ctypes.byref(cmaxSamples),
		segFrom,
		segTo,
		1,
		config['PicoRatioMode'],
		ctypes.byref(overflow)
	))

	print("[{:.4f} s] Bulk values read out ({} -> {})".format(time.time() - startTime, segFrom, segTo))

	print("Data readout time: {}".format(time.time() - readOutTime))

	# Save data
	with open(datapath.format(0, trigName, segFrom, segTo), "wb") as file:
		for i in range(config['nWaveforms']):
			file.write(bufferAMax[i])
						

	print("[{:.4f} s] Dataframes saved".format(time.time() - startTime))

	#print(time.time()-start)
	# Closes the unit
	# Handle = chandle
	assert_pico_ok(ps.ps6000aCloseUnit(chandle))
	print("pico closed")

sys.exit()