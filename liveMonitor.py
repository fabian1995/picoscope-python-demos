import ctypes
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import time
import os.path

#run filepath
filepath="../../../../media/ilcuser/T7/DESY_Timing_Study/test/run2/Bulk{}-Ch{}-wf{}-{}.bin"
#number of bulks/loops for run
loops=6000
nWaveforms = 10000
maxsamples = 3000

averages = np.zeros(nWaveforms)
segFrom = 0
segTo = nWaveforms-1

offset = 950
offset_adc = offset*32512/1000

adc_to_pe = 1000/(32512*-13.5)

xmin = 0#900
xmax = 2999#1100
xaxis = np.arange(xmin, xmax+1) 
subplots = 10
plt.ion()

for loop in range(loops):
	while not os.path.isfile(filepath.format(loop,'A',segFrom, segTo)):
		time.sleep(1)
	if os.path.isfile(filepath.format(loop,'A',segFrom, segTo)):
		startTime = time.time()

		# Read DataFrames
		bufferType = ctypes.c_int16 * maxsamples

		readBufferA = np.zeros((nWaveforms, maxsamples), dtype=np.int16)

		with open(filepath.format(loop, "A", segFrom, segTo), "rb") as file:
			for i in range(nWaveforms):
				content = file.read(ctypes.sizeof(bufferType))
				pointer = ctypes.cast(content, ctypes.POINTER(bufferType))
				readBufferA[i, :] = np.asarray(pointer.contents)

		readBufferC = np.zeros((nWaveforms, maxsamples), dtype=np.int16)

		with open(filepath.format(loop, "C", segFrom, segTo), "rb") as file:
			for i in range(nWaveforms):
				content = file.read(ctypes.sizeof(bufferType))
				pointer = ctypes.cast(content, ctypes.POINTER(bufferType))
				readBufferC[i, :] = np.asarray(pointer.contents)

	
		readBufferE = np.zeros((nWaveforms, maxsamples), dtype=np.int16)

		with open(filepath.format(loop, "E", segFrom, segTo), "rb") as file:
			for i in range(nWaveforms):
				content = file.read(ctypes.sizeof(bufferType))
				pointer = ctypes.cast(content, ctypes.POINTER(bufferType))
				readBufferE[i, :] = np.asarray(pointer.contents)

		readBufferG = np.zeros((nWaveforms, maxsamples), dtype=np.int16)

		with open(filepath.format(loop, "G", segFrom, segTo), "rb") as file:
			for i in range(nWaveforms):
				content = file.read(ctypes.sizeof(bufferType))
				pointer = ctypes.cast(content, ctypes.POINTER(bufferType))
				readBufferG[i, :] = np.asarray(pointer.contents)

		readBufferA = (readBufferA-offset_adc)*adc_to_pe		
		readBufferC = (readBufferC-offset_adc)*adc_to_pe		
		readBufferE = (readBufferE-offset_adc)*adc_to_pe		
		readBufferG = (readBufferG-offset_adc)*adc_to_pe		
				
		plt.close()
		plt.figure("Waveform Bulk Number {}".format(loop), figsize=(16,9))
		
		#plot every 1000nd waveform of the bulk
		for i in range(subplots):
			plt.subplot(subplots, 1, i+1)
			plt.plot(xaxis*0.4, readBufferA[i*1000, :], label="Ch A")
			plt.plot(xaxis*0.4, readBufferC[i*1000, :], label="Ch C")
			plt.plot(xaxis*0.4, readBufferE[i*1000, :], label="Ch E")
			plt.plot(xaxis*0.4, readBufferG[i*1000, :], label="Ch G")
			if i==4 :
				plt.ylabel("Signal height [pe]", loc='center', fontsize=20)
			
		plt.xlabel("Time [ns]", fontsize=20)
		

		plt.legend()
		plt.show()
		plt.pause(0.001)
