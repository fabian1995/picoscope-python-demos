from picosdk.functions import mV2adc as pico_mV2adc, adc2mV as pico_adc2mV
from PicoUtils import mV2adc as my_mV2adc, adc2mV as my_adc2mV
import ctypes

maxADC = ctypes.c_int16(32512)

voltage = 20
chRange = 3

result = pico_mV2adc(voltage, chRange, maxADC)
result2 = my_mV2adc(voltage, chRange)

print(result, result2)