import ctypes
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import time
from lmfit.models import GaussianModel
from lmfit import fit_report
import json

# rootpath = "1pe-Cal/BC408_fullRange/run56-fullRange-v4/"
# rootpath = "1pe-Cal/AHCAL/run1-1peCal/"
rootpath = "Autoruns/AHCAL-2020-10-21-evening/run178-cal1pe-v4/"

from Cal1pe import Cal1peAnalysis

cal = Cal1peAnalysis(rootpath, 450, 650)

for channelName in ["A", "C", "E", "G"]:
	cal.analyzeChannel(channelName)
	cal.plotChannelSample(channelName)
	cal.generateHistAndFit(channelName, "1pe", -300, 200, True, True)
	cal.generateHistAndFit(channelName, "1pe-height", None, 100, True, True)
	cal.generateHistAndFit(channelName, "pedestal", None, 100, True, True)

plt.show()

exit()
