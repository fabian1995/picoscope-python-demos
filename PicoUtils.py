def picoMaxVoltage(chRange):
	#10mV [0], 20mV [1], 50mV [2], 100mV [3], 200mV [4], 500mV [5]
	if chRange == 0: 	return    10
	elif chRange == 1:	return    20
	elif chRange == 2:	return    50
	elif chRange == 3:	return   100
	elif chRange == 4:	return   200
	elif chRange == 5:	return   500
	elif chRange == 6:	return  1000
	elif chRange == 7:	return  2000
	elif chRange == 8:	return  5000
	elif chRange == 9:	return 10000
	elif chRange == 10:	return 20000
	elif chRange == 11:	return 50000
	else: raise(Exception("Invalid voltage range: {}".format(chRange)))


def mV2adc(voltage, chRange, maxADC=32512):
	maxVoltage = picoMaxVoltage(chRange)	
	return maxADC * voltage / maxVoltage


def adc2mV(counts, chRange, maxADC=32512):
	maxVoltage = picoMaxVoltage(chRange)
	return maxVoltage * counts / maxADC


def picoTimeScale_ns(timebase):
	if timebase == 0:	return 0.2
	elif timebase == 1: return 0.4
	elif timebase == 2: return 0.8
	elif timebase == 3: return 1.6
	elif timebase == 4: return 3.2
	elif timebase == 5: return 6.4
	else: raise(Exception("Invalid timebase: {}".format(timebase)))