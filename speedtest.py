import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

data = np.asarray([
	[0.5, 25.08],
	[0.8, 15.96],
	[1, 12.7],
	[2, 6.38],
	[3, 4.27],
	[5, 2.56],
	[10, 1.28],
	[20, 0.66],
	[30, 0.44],
	[50, 0.28]
])

df = pd.DataFrame(data=data, columns=["frequency", "DAQspeed"])

plt.title("Acquiring 10k waveforms @ 3k samples each [1.2us], 2 channels")

plt.plot(data[:, 0], data[:, 1], "o--", label="DAQ")
plt.axhline(3.77, color="g", label="Download")
plt.axhline(3.77+0.45, color="g", linestyle="--", label="Download + save bin")

plt.axhline(11.0, color="orange", label="Download + convert")
plt.axhline(11.27, color="r", label="Download + Save (np pickle)")

plt.xscale("log")
plt.yscale("log")

plt.xlabel("Input frequency [kHz]")
plt.ylabel("Time (s)")

plt.legend()

plt.show()