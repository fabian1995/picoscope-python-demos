import ctypes
from picosdk.ps6000 import ps6000 as ps
import numpy as np
from picosdk.functions import mV2adc, adc2mV, assert_pico_ok
import time
import pandas as pd

from picosdk.discover import find_all_units

scopes = find_all_units()

for scope in scopes:
    print(scope.info)
    scope.close()

# Number of waveforms to be recorded
nWaveforms = 1000
nBulks = 1

filepath = "waveforms/scan1-Ch{}-wf{}-{}.pkl"

# Create chandle and status ready for use
status = {}

# Enumerate PicoScope Units
count = ctypes.c_int16(0)
serials = ctypes.c_int8(0)
serialLth = ctypes.c_int16(0)

ps.ps6000EnumerateUnits(ctypes.byref(count), ctypes.byref(serials), ctypes.byref(serialLth))
print("Found {} PicoScope units".format(count.value))

# Opens the device/s
chandle = ctypes.c_int16()
# serialPy = "HT453/0070"
# b_serial = serialPy.encode('utf-8')
# c_serial = ctypes.c_char_p(b_serial)
status["openunit"] = ps.ps6000OpenUnit(ctypes.byref(chandle), None)
assert_pico_ok(status["openunit"])

# Displays the serial number and handle
print("Connected to PicoScope, handle value = {}".format(chandle.value))

# Set up channel A
# handle = chandle
# channel = ps6000_CHANNEL_A = 0
# enabled = 1
# coupling type = ps6000_AC = 0
# range = ps6000_50MV = 3
# analogue offset = 0 V
chARange = 4
chACoupling = 2
status["setChA"] = ps.ps6000SetChannel(chandle, 0, 1, chACoupling, chARange, 0, 0)
assert_pico_ok(status["setChA"])

# Set up channel B
# handle = chandle
# channel = ps6000_CHANNEL_B = 1
# enabled = 1
# coupling type = ps6000_AC = 0
# range = ps6000_50MV = 3
# analogue offset = 0 V
status["setChC"] = ps.ps6000SetChannel(chandle, 2, 1, chACoupling, chARange, 0, 0)
assert_pico_ok(status["setChC"])

# Set logic trigger --> coincidence
# These conditions produce a logical AND
triggerConditions = ps.PS6000_TRIGGER_CONDITIONS(
	ps.PS6000_TRIGGER_STATE["PS6000_CONDITION_TRUE"],
	ps.PS6000_TRIGGER_STATE["PS6000_CONDITION_DONT_CARE"],
	ps.PS6000_TRIGGER_STATE["PS6000_CONDITION_TRUE"],
	ps.PS6000_TRIGGER_STATE["PS6000_CONDITION_DONT_CARE"],
	ps.PS6000_TRIGGER_STATE["PS6000_CONDITION_DONT_CARE"],
	ps.PS6000_TRIGGER_STATE["PS6000_CONDITION_DONT_CARE"],
	ps.PS6000_TRIGGER_STATE["PS6000_CONDITION_DONT_CARE"]
)

nTriggerConditions = 1
status["setTriggerChannelConditions"] = ps.ps6000SetTriggerChannelConditions(chandle, ctypes.byref(triggerConditions), nTriggerConditions)
assert_pico_ok(status["setTriggerChannelConditions"])

status["setTriggerChannelDirections"] = ps.ps6000SetTriggerChannelDirections(chandle, 
	ps.PS6000_THRESHOLD_DIRECTION["PS6000_FALLING"], 
	ps.PS6000_THRESHOLD_DIRECTION["PS6000_NONE"],
	ps.PS6000_THRESHOLD_DIRECTION["PS6000_FALLING"], 
	ps.PS6000_THRESHOLD_DIRECTION["PS6000_NONE"], 
	ps.PS6000_THRESHOLD_DIRECTION["PS6000_NONE"],
	ps.PS6000_THRESHOLD_DIRECTION["PS6000_NONE"]
)
assert_pico_ok(status["setTriggerChannelDirections"])

maxADC = ctypes.c_int16(32512)
threshold_mV = -100
threshold = mV2adc(threshold_mV, chARange, maxADC)
hysteresis = mV2adc(int(np.abs(threshold_mV) * 0.015), chARange, maxADC)
print("Threshold: {}".format(threshold))

struct_arr = (ps.PS6000_TRIGGER_CHANNEL_PROPERTIES * 2)()
struct_arr[0] = ps.PS6000_TRIGGER_CHANNEL_PROPERTIES(
	threshold,
	hysteresis,
	threshold,
	hysteresis,
	ps.PS6000_CHANNEL["PS6000_CHANNEL_A"],
	ps.PS6000_THRESHOLD_MODE["PS6000_LEVEL"]
)
struct_arr[1] = ps.PS6000_TRIGGER_CHANNEL_PROPERTIES(
	threshold,
	hysteresis,
	threshold,
	hysteresis,
	ps.PS6000_CHANNEL["PS6000_CHANNEL_C"],
	ps.PS6000_THRESHOLD_MODE["PS6000_LEVEL"]
)

nChannelProperties = 2
auxOutputEnable = 0
autoTriggerMilliseconds = 0
status["setTriggerChannelProperties"] = ps.ps6000SetTriggerChannelProperties(chandle, ctypes.byref(struct_arr), nChannelProperties, auxOutputEnable, autoTriggerMilliseconds)
assert_pico_ok(status["setTriggerChannelProperties"])
# status["setTriggerChannelProperties"] = ps.ps6000SetTriggerChannelProperties(chandle, ctypes.byref(channelPropertiesB), nChannelProperties, auxOutputEnable, autoTriggerMilliseconds)
# assert_pico_ok(status["setTriggerChannelProperties"])

# Setting the number of sample to be collected
preTriggerSamples = 1000
postTriggerSamples = 2000
maxsamples = preTriggerSamples + postTriggerSamples

# Gets timebase innfomation
# Handle = chandle
# Timebase = 2 = timebase
# Nosample = maxsamples
# TimeIntervalNanoseconds = ctypes.byref(timeIntervalns)
# Oversample = 1
# MaxSamples = ctypes.byref(returnedMaxSamples)
# Segement index = 0
timebase = 2
timeIntervalns = ctypes.c_float()
returnedMaxSamples = ctypes.c_int32()
status["GetTimebase"] = ps.ps6000GetTimebase2(chandle, timebase, maxsamples, ctypes.byref(timeIntervalns), 1, ctypes.byref(returnedMaxSamples), 0)
assert_pico_ok(status["GetTimebase"])
print("Time interval (ns) = {}, max samples = {}".format(timeIntervalns.value, returnedMaxSamples.value))

# Creates a overlow location for data
overflow = ctypes.c_int16()
# Creates converted types maxsamples
cmaxSamples = ctypes.c_int32(maxsamples)

status["MemorySegments"] = ps.ps6000MemorySegments(chandle, nWaveforms, ctypes.byref(cmaxSamples))
assert_pico_ok(status["MemorySegments"])

# sets number of captures
status["SetNoOfCaptures"] = ps.ps6000SetNoOfCaptures(chandle, nWaveforms)
assert_pico_ok(status["SetNoOfCaptures"])

startTime = time.time()
print("[{:.4f} s] Setting buffers".format(time.time() - startTime))

bufferAMax = []
bufferAMin = []
bufferBMax = []
bufferBMin = []

bulkSize = int(nWaveforms/nBulks)
for wf in range(bulkSize):
	# Create buffers ready for assigning pointers for data collection
	# bufferAMax["wf{}".format(wf)] = (ctypes.c_int16 * maxsamples)()
	# bufferAMin["wf{}".format(wf)] = (ctypes.c_int16 * maxsamples)() # used for downsampling which isn't in the scope of this example
	bufferAMax.append((ctypes.c_int16 * maxsamples)())
	bufferAMin.append((ctypes.c_int16 * maxsamples)())
	bufferBMax.append((ctypes.c_int16 * maxsamples)())
	bufferBMin.append((ctypes.c_int16 * maxsamples)())

	# Setting the data buffer location for data collection from channel A
	# Handle = Chandle
	# source = ps6000_channel_A = 0
	# Buffer max = ctypes.byref(bufferAMax)
	# Buffer min = ctypes.byref(bufferAMin)
	# Buffer length = maxsamples
	# Segment index = 0
	# Ratio mode = ps6000_Ratio_Mode_None = 0
	
	# print("WF: {}, Bulksize: {}".format(wf, bulkSize))
	for bulk in range(nBulks):
		# print(wf+bulkSize*bulk, end=" ")
		status["SetDataBuffersBulk{}".format(wf)] = ps.ps6000SetDataBuffersBulk(chandle, 0, ctypes.byref(bufferAMax[wf]), ctypes.byref(bufferAMin[wf]), maxsamples, wf+bulkSize*bulk, 0)
		assert_pico_ok(status["SetDataBuffersBulk{}".format(wf)])
		status["SetDataBuffersBulk{}".format(wf)] = ps.ps6000SetDataBuffersBulk(chandle, 2, ctypes.byref(bufferBMax[wf]), ctypes.byref(bufferBMin[wf]), maxsamples, wf+bulkSize*bulk, 0)
		assert_pico_ok(status["SetDataBuffersBulk{}".format(wf)])

	# print()

print("[{:.4f} s] Starting block capture".format(time.time()-startTime))

# Starts the block capture
# Handle = chandle
# Number of prTriggerSamples
# Number of postTriggerSamples
# Timebase = 2 = 4ns (see Programmer's guide for more information on timebases)
# time indisposed ms = None (This is not needed within the example)
# Segment index = 0
# LpRead = None
# pParameter = None
status["runblock"] = ps.ps6000RunBlock(chandle, preTriggerSamples, postTriggerSamples, timebase, 1, None, 0, None, None)
assert_pico_ok(status["runblock"])

print("[{:.4f} s] Waiting for scope to be ready".format(time.time() - startTime))


# Creates a overlow location for data
overflow = (ctypes.c_int16 * 10)()
# Creates converted types maxsamples
cmaxSamples = ctypes.c_int32(maxsamples)

# Checks data collection to finish the capture
ready = ctypes.c_int16(0)
check = ctypes.c_int16(0)
while ready.value == check.value:
    status["isReady"] = ps.ps6000IsReady(chandle, ctypes.byref(ready))


# Read out blocks in bulks
bulkSize = int(nWaveforms/nBulks)

for bulk in range(nBulks):
	segFrom = bulkSize * bulk
	segTo = bulkSize * (bulk+1) - 1
	# Handle = chandle
	# noOfSamples = ctypes.byref(cmaxSamples)
	# fromSegmentIndex = 0
	# ToSegmentIndex = 9
	# DownSampleRatio = 0
	# DownSampleRatioMode = 0
	# Overflow = ctypes.byref(overflow)
	status["GetValuesBulk"] = ps.ps6000GetValuesBulk(chandle, ctypes.byref(cmaxSamples), segFrom, segTo, 0, 0, ctypes.byref(overflow))
	assert_pico_ok(status["GetValuesBulk"])

	print("[{:.4f} s] Bulk values read out ({} -> {})".format(time.time() - startTime, segFrom, segTo))

	# Save data
	df = pd.DataFrame(data=np.transpose(np.asarray(bufferAMax)), columns=np.arange(segFrom, segTo+1, 1))
	df.to_pickle(filepath.format("A", segFrom, segTo))

	df = pd.DataFrame(data=np.transpose(np.asarray(bufferBMax)), columns=np.arange(segFrom, segTo+1, 1))
	df.to_pickle(filepath.format("B", segFrom, segTo))

print("[{:.4f} s] Dataframes saved".format(time.time() - startTime))


# Handle = chandle
# Times = Times = (ctypes.c_int16*10)() = ctypes.byref(Times)
# Timeunits = TimeUnits = ctypes.c_char() = ctypes.byref(TimeUnits)
# Fromsegmentindex = 0
# Tosegementindex = 9
# Times = (ctypes.c_int16*10)()
# TimeUnits = ctypes.c_char()
# status["GetValuesTriggerTimeOffsetBulk"] = ps.ps6000GetValuesTriggerTimeOffsetBulk64(chandle, ctypes.byref(Times), ctypes.byref(TimeUnits), 0, nWaveforms-1)
# assert_pico_ok(status["GetValuesTriggerTimeOffsetBulk"])

# print("[{:.4f} s] All values read out".format(time.time() - startTime))

# Finds the max ADC count
# maxADC = ctypes.c_int16(32512)

# # Creates the time data
# timeInt = timeIntervalns.value
# maxSam = cmaxSamples.value

# for wf in range(min(10, nWaveforms)):
# 	# Converts ADC from channel A to mV
# 	adc2mVChAMax = adc2mV(bufferAMax[wf], chARange, maxADC)

# 	# Plots the data from channel A onto a graph
# 	plt.plot(time, bufferAMax[wf], label="Waveform {}".format(wf))

# plt.xlabel('Time (ns)')
# plt.ylabel('Voltage (mV)')

# plt.legend()

# plt.show()

# Stops the scope
# Handle = chandle
# status["stop"] = ps.ps6000Stop(chandle)
# assert_pico_ok(status["stop"])

# Closes the unit
# Handle = chandle
status["close"] = ps.ps6000CloseUnit(chandle)
assert_pico_ok(status["close"])

# Displays the staus returns
# print("Picoscope status logs:")
# print(status)
