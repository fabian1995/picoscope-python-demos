import ctypes
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import time
from lmfit.models import GaussianModel
from lmfit import fit_report
import pickle
import os.path
import json

#Applies pedestal correction and integrates a single waveform
def WfIntegralCorr(bufferData, wf_per_bunch, samples, preTrig):
    data2 = bufferData.astype('float64')
    average2 = np.average(data2[:, :preTrig], axis=1)
    data2 -= average2[:, None]
    sums2 = np.sum(data2[:, :wf_per_bunch], axis=1)
    return sums2


#Returns a list to a pickle for each of the channels(0->A, 1->C, 2->E, 3->G) with the integrated values 
#of all waveforms in ADC, pedestally corrected

#Rootpath -> path with all run folders
#startRun -> first run to be analysed
#endRun -> last run to be analysed

def ReadEvents(rootpath, startRun, endRun):
    
    segFrom=0

    chA=[]
    chC=[]
    chE=[]
    chG=[]

    filepath = rootpath+"/run{}-coincidence/Bulk{}-Ch{}-wf{}-{}.bin"
    filepath_test=rootpath+"/run{}-coincidence"

    for run in range((endRun-startRun)):
        if os.path.isdir(filepath_test.format(run+startRun)):
            #start=time.time()
            config_path=filepath_test.format(run+startRun)+"/config.json"
            with open(config_path) as json_file:
                data=json.load(json_file)
            wf_per_bunch=data["nWaveforms"]
            segTo=wf_per_bunch-1
            loops=data["loops"]
            samples=data["preTriggerSamples"]+data["postTriggerSamples"]
            for loop in range(loops):
                if os.path.isfile(filepath.format(run+startRun, loop, "A", segFrom, segTo)):
                    bufferType = ctypes.c_int16 * samples
                    readBufferA = np.zeros((wf_per_bunch, samples), dtype=np.int16)

                    with open(filepath.format(run+startRun, loop, "A", segFrom, segTo), "rb") as file:
                        for i in range(wf_per_bunch):
                            content = file.read(ctypes.sizeof(bufferType))
                            pointer = ctypes.cast(content, ctypes.POINTER(bufferType))
                            readBufferA[i, :] = np.asarray(pointer.contents)

                    readBufferC = np.zeros((wf_per_bunch, samples), dtype=np.int16)

                    with open(filepath.format(run+startRun, loop, "C", segFrom, segTo), "rb") as file:
                        for i in range(wf_per_bunch):
                            content = file.read(ctypes.sizeof(bufferType))
                            pointer = ctypes.cast(content, ctypes.POINTER(bufferType))
                            readBufferC[i, :] = np.asarray(pointer.contents)

                    readBufferE = np.zeros((wf_per_bunch, samples), dtype=np.int16)

                    with open(filepath.format(run+startRun, loop, "E", segFrom, segTo), "rb") as file:
                        for i in range(wf_per_bunch):
                            content = file.read(ctypes.sizeof(bufferType))
                            pointer = ctypes.cast(content, ctypes.POINTER(bufferType))
                            readBufferE[i, :] = np.asarray(pointer.contents)

                    readBufferG = np.zeros((wf_per_bunch, samples), dtype=np.int16)

                    with open(filepath.format(run+startRun, loop, "G", segFrom, segTo), "rb") as file:
                        for i in range(wf_per_bunch):
                            content = file.read(ctypes.sizeof(bufferType))
                            pointer = ctypes.cast(content, ctypes.POINTER(bufferType))
                            readBufferG[i, :] = np.asarray(pointer.contents)
            
                    dataAsum = WfIntegralCorr(readBufferA,wf_per_bunch,samples,data["preTriggerSamples"]-50)
                    chA.append(dataAsum)
                    dataCsum = WfIntegralCorr(readBufferC,wf_per_bunch,samples,data["preTriggerSamples"]-50)
                    chC.append(dataCsum)
                    dataEsum = WfIntegralCorr(readBufferE,wf_per_bunch,samples,data["preTriggerSamples"]-50)
                    chE.append(dataEsum)
                    dataGsum = WfIntegralCorr(readBufferG,wf_per_bunch,samples,data["preTriggerSamples"]-50)
                    chG.append(dataGsum)
            print("done with run{}".format(run+startRun))
           # print(time.time()-start)
    #start=time.time()
    chA_list=[]
    chC_list=[]
    chE_list=[]
    chG_list=[]

    for sublist in chA:
        for item in sublist:
            chA_list.append(item)

    for sublist in chC:
        for item in sublist:
            chC_list.append(item)

    for sublist in chE:
        for item in sublist:
            chE_list.append(item)
    
    for sublist in chG:
        for item in sublist:
            chG_list.append(item)
    end=time.time()
    
    return chA_list,chC_list,chE_list,chG_list
    

#converts list to array, does two-fold fit to a gaussian and returns same array converted to MIP, also saves it to a pickle

def ADCtoMIP(input_list, channel):
    #print("1")
    arr=np.array(input_list)
    hist, bins_edges=np.histogram(arr, bins=300, range=[-1000000,0])
    xbins = (bins_edges[1:] + bins_edges[:-1]) / 2.

    gmodel = GaussianModel()
    midGuess = xbins[np.argmax(hist)]
    stdGuess = np.sqrt(np.average((xbins - midGuess)**2, weights=hist))
    params = gmodel.make_params(amplitude=hist[np.argmax(hist)], center=midGuess, sigma=stdGuess)
    result = gmodel.fit(hist, params, x=xbins)

    lowerbound = result.params["center"] - 1.5 * result.params["sigma"]
    upperbound = result.params["center"] + 1.5 * result.params["sigma"]
    mask = np.logical_and.reduce([xbins > lowerbound, xbins < upperbound])

    gmodel2 = GaussianModel()
    params2 = gmodel.make_params(amplitude=result.params["amplitude"], center=result.params["center"], sigma=result.params["sigma"])
    result2 = gmodel.fit(hist[mask], params2, x=xbins[mask])
    print("MIP Value for channel {} in ADC counts: \n\tvalue\t{} \n\tstd\t {}".format(channel, result2.params["center"], result2.params["sigma"]))
    #lowerbound2 = result.params2["center"] - 2 * result.params2["sigma"]
    #upperbound2 = result.params2["center"] + 2 * result.params2["sigma"]
    #mask2 = np.logical_and.reduce([xbins > lowerbound2, xbins < upperbound2])

    #gmodel3 = GaussianModel()
    #params3 = gmodel.make_params(amplitude=result2.params["amplitude"], center=result2.params["center"], sigma=result2.params["sigma"])
    #result3 = gmodel.fit(hist[mask2], params3, x=xbins[mask2])
    #arr=np.array(input_list)
    channel_mip=arr/result2.params["center"]
    
    with open("ch{}_MIP.pkl".format(channel),'wb') as a:
        pickle.dump(channel_mip, a)
    
    return(channel_mip)

#channel=ReadEvents("/home/iwsatlas1/popov/NAS_futDet/DESY_Timing_Study/Autoruns/AHCAL-2020-10-21-evening",4,200)
#channelA_MIP=ADCtoMIP(channel[0],'chA_mip.pdf')
#channelC_MIP=ADCtoMIP(channel[1],'chA_mip.pdf')
#channelE_MIP=ADCtoMIP(channel[2],'chA_mip.pdf')
#channelG_MIP=ADCtoMIP(channel[3],'chA_mip.pdf')
