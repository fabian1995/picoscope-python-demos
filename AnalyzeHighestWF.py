import ctypes
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import time
import scipy.signal as sig
import scipy.integrate as sci

from WaveformReader import Run, MainPeak

# Parameters
analysisThreshold = 500
subpeak_prominences = [20]#range(15, 24, 3)
pulseOver = 600

# Initialize run
run = Run("Autoruns/AHCAL-2020-10-21-evening/run189-coincidence/")

# Read out one waveform bulk
chA1 = run.readBulk(76, "A", pedestal=400)
xaxis = run.getXaxis()

highestSignal = 0
highestSignalIndex = 0

for wf, signal in enumerate(-chA1[:,:]):
	if np.max(signal) > highestSignal:
		highestSignal = np.max(signal)
		highestSignalIndex = wf

# highestSignalIndex = 5923
plt.plot(xaxis, -chA1[highestSignalIndex, :])

plt.show()