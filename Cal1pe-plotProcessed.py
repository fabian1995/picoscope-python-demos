import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import time

folder1pe = "1pe-Cal/AHCAL/"
folder1pe = "1pe-Cal/BC408_fullRange/"
folder1pe = "Autoruns/AHCAL-2020-10-21-evening/"

outfile = folder1pe + "processed.csv"

df = pd.read_csv(outfile)

channels = ["A", "C", "E", "G"]

# Make time axis more readable
startTime = np.min(df["timeAxis"])
df["timeAxis"] -= startTime
df["timeAxis"] /= 3600

# Plot 1 p.e. data
plt.figure("1pe values")

for j, ch in enumerate(channels):
	mask = df["err{}".format(ch)] < 1000
	plt.errorbar(df["timeAxis"][mask], df["val{}".format(ch)][mask], fmt=".", yerr=df["err{}".format(ch)][mask], capsize=2, label="Ch {}".format(ch))

plt.legend()

stime = time.gmtime(startTime)
plt.xlabel("Time in hours since first run ({}-{:02d}-{:02d} {:02d}:{:02d})".format(stime.tm_year, stime.tm_mon, stime.tm_mday, stime.tm_hour, stime.tm_min))
plt.ylabel("Integrated 1pe signal (mV$\\cdot$ns)")

# Plot pedestal data
plt.figure("pedestal values")

for j, ch in enumerate(channels):
	plt.errorbar(df["timeAxis"], df["valPed{}".format(ch)], fmt=".", yerr=df["errPed{}".format(ch)], capsize=2, label="Ch {}".format(ch))

plt.legend()

plt.xlabel("Time in hours since first run ({}-{:02d}-{:02d} {:02d}:{:02d})".format(stime.tm_year, stime.tm_mon, stime.tm_mday, stime.tm_hour, stime.tm_min))
plt.ylabel("Pedestal voltage (mV)")

# Show plot and finish
plt.show()