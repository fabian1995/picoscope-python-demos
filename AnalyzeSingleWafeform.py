import ctypes
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import time
import scipy.signal as sig
import scipy.integrate as sci

from WaveformReader import Run, MainPeak

# Parameters
analysisThreshold = 500

# Initialize run
run5 = Run("testdata/run5/")

maxSamples = run5.maxsamples

trigger_mV = - run5.config["threshold_mV"] + run5.config["analogueOffset"] * 1000

wfnumber = 0#326

# Read out one waveform bulk
chA1 = run5.readBulk(0, "A", pedestal=900)
xaxis = run5.getXaxis()
signal = -chA1[wfnumber,:]

peaks, properties = sig.find_peaks(signal, height=15, prominence=20)

print(properties)

# properties["right_bases"][0]
# peaks[0]+200
mp = MainPeak(peaks[0], properties["left_bases"][0], maxSamples-1, properties["peak_heights"][0], xaxis, signal)

plt.figure("Waveform")

plt.plot(xaxis, signal)

for peak, lb, rb in zip(peaks, properties["left_bases"], properties["right_bases"]):
	plt.axvline(xaxis[peak], color="r", linewidth=0.75)
	# plt.axvline(xaxis[lb], color=c, linewidth=0.75, linestyle=":")
	# plt.axvline(xaxis[rb], color=c, linewidth=0.75, linestyle=":")

# plt.axhline(threshold_mV, color="cyan")

do_fit = True

xnew, ynew = mp.fitAndIntersect(trigger_mV, plot=True, do_fit=do_fit)

if not do_fit:
	plt.show()
	exit()

ynew[ynew < 0] = 0

peaks, properties = sig.find_peaks(ynew, height=5, prominence=10)
print(len(peaks))

plt.plot(xnew, ynew)

for peak, lb, rb in zip(peaks, properties["left_bases"], properties["right_bases"]):
	plt.axvline(xnew[peak], color="r", linewidth=0.75)
	integral = sci.simps(ynew[lb:rb+1], xnew[lb:rb+1])
	print("Integral: {}".format(integral))

	plt.plot(xnew[lb:rb+1], ynew[lb:rb+1])

# for peak, lb, rb in zip(peaks, properties["left_bases"], properties["right_bases"]):
# 	plt.axvline(xnew[peak], color="r", linewidth=0.75)
# 	# plt.axvline(xnew[lb], color=c, linewidth=0.75, linestyle=":")
# 	# plt.axvline(xnew[rb], color=c, linewidth=0.75, linestyle=":")
# 	integral = sci.simps(ynew[lb:rb+1], xnew[lb:rb+1])
# 	print("Integral: {}".format(integral))
# 	plt.plot(xnew[lb:rb+1], ynew[lb:rb+1], color="r")

plt.figure("Waveform")
for peak, lb, rb in zip(peaks, properties["left_bases"], properties["right_bases"]):
	plt.axvline(xnew[peak], color="r", linewidth=0.75)
	plt.axvline(xnew[lb], color="r", linewidth=0.75, linestyle=":")
	plt.axvline(xnew[rb], color="r", linewidth=0.75, linestyle=":")
	

plt.show()