import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

# Number of waveforms to be recorded
nWaveforms = 10000
nBulks = 10

filepath = "waveforms/scan1-Ch{}-wf{}-{}.pkl"

# Read DataFrames
bulkSize = int(nWaveforms/nBulks)
averages = np.zeros(nWaveforms)
for bulk in range(nBulks):
	segFrom = bulkSize * bulk
	segTo = bulkSize * (bulk+1) - 1
	dfA = pd.read_pickle(filepath.format("A", segFrom, segTo))
	dfB = pd.read_pickle(filepath.format("B", segFrom, segTo))

	averages[segFrom:segTo+1] = dfA.mean(axis=0)

	# if bulk < 3:
	# 	plt.figure("Waveform {}".format(segFrom))
	# 	plt.plot(np.arange(dfA.shape[0]), dfA[segFrom], label="Ch A")
	# 	plt.plot(np.arange(dfB.shape[0]), dfB[segFrom], label="Ch B")

	xmin = 0#900
	xmax = 2999#1100
	xaxis = np.arange(xmin, xmax+1) 
	subplots = 10
	if bulk < 3:
		plt.figure("Waveform {}".format(bulk))
		for i in range(subplots):
			plt.subplot(subplots, 1, i+1)
			plt.plot(xaxis, dfA.loc[xmin:xmax,bulk*bulkSize+i], label="Ch A")
			plt.plot(xaxis, dfB.loc[xmin:xmax,bulk*bulkSize+i], label="Ch B")

plt.legend()

# plt.figure("Baseline")
# plt.plot(np.arange(nWaveforms), averages)

plt.show()