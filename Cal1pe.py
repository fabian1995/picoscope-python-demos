import ctypes
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import time
from lmfit.models import GaussianModel
from lmfit import fit_report
import json
from PicoUtils import adc2mV

class Cal1peAnalysis:
	def __init__(self, runpath, baseline, signalend):
		self.runpath = runpath
		self.baseline = baseline
		self.signalend = signalend
		self.val1pe = {}
		self.pedestals = {}
		self.channelData = {}
		self.samplesPlotted = False

		with open(runpath + "config.json") as json_file: 
			self.config = json.load(json_file)

		self.maxsamples = self.config["preTriggerSamples"] + self.config["postTriggerSamples"]
		self.bufferType = ctypes.c_int16 * self.maxsamples


	def get1peValue(self, bufferData):
		pedestal = np.average(bufferData[:, :self.baseline], axis=1)

		data = bufferData.astype('float64')
		data -= pedestal[:, None]

		sums = np.sum(data[:, :self.signalend], axis=1) * self.config["timeIntervalns"]

		return data, sums, pedestal


	def analyzeChannel(self, channelName):
		readBuffer = np.zeros((self.config["nWaveforms"], self.maxsamples), dtype=np.int16)
		filepath = self.runpath + "Bulk0-Ch{}-wf{}-{}.bin"

		with open(filepath.format(channelName, 0, self.config["nWaveforms"]-1), "rb") as file:
			for i in range(self.config["nWaveforms"]):
				content = file.read(ctypes.sizeof(self.bufferType))
				pointer = ctypes.cast(content, ctypes.POINTER(self.bufferType))
				readBuffer[i, :] = np.asarray(pointer.contents)

		voltages = adc2mV(readBuffer.astype('float64'), self.config["channelRange"], self.config["maxADC"])
		self.channelData[channelName], self.val1pe[channelName], self.pedestals[channelName] = self.get1peValue(voltages)


	def plotChannelSample(self, channelName):
		# Plot some waveforms 
		xmin = 0#900
		xmax = self.maxsamples#1100
		xaxis = np.arange(xmin, xmax) * self.config["timeIntervalns"]
		subplots = 4
		offset = 0

		plt.figure("First 10 Waveforms")
		for i, j in zip(range(subplots), range(offset, offset+subplots)):
			plt.subplot(subplots, 1, i+1)
			plt.plot(xaxis, self.channelData[channelName][j, :], label="Ch {}".format(channelName))

			if not self.samplesPlotted:
				plt.axvline(self.baseline * self.config["timeIntervalns"], linewidth=0.75)
				plt.axvline(self.signalend * self.config["timeIntervalns"], linewidth=0.75)

		plt.legend()

		self.samplesPlotted = True


	def generateHistAndFit(self, channelName, dataset="1pe", minValue=None, bins=50, plot=False, verbose=False):
		if dataset == "1pe":
			data = self.val1pe[channelName]
			unit = "mV$\\cdot$ns"
		elif dataset == "1pe-height":
			data = np.min(self.channelData[channelName][self.baseline:self.signalend], axis=1)
			unit = "mV"
		else:
			data = self.pedestals[channelName]
			unit = "mV"

		if plot: plt.figure("{}".format(dataset))

		if minValue is not None:
			data = data[data > minValue]

		hist, bins = np.histogram(data, bins=bins, density=False)
		xbins = (bins[1:] + bins[:-1]) / 2.

		gmodel = GaussianModel()
		midGuess = np.average(xbins, weights=hist)
		stdGuess = np.sqrt(np.average((xbins - midGuess)**2, weights=hist))
		amplGuess = np.max(hist) * stdGuess
		if verbose: print("Channel {}/{} initial guesses: ampl = {}, mu = {}, sigma = {}".format(channelName, dataset, amplGuess, midGuess, stdGuess))
		params = gmodel.make_params(amplitude=amplGuess, center=midGuess, sigma=stdGuess)
		# plt.plot(xbins, gmodel.eval(params=params, x=xbins), ".--", color="r")
		result = gmodel.fit(hist, params, x=xbins)

		lowerbound = result.params["center"] - 3 * result.params["sigma"]
		upperbound = result.params["center"] + 3 * result.params["sigma"]
		mask = np.logical_and.reduce([xbins > lowerbound, xbins < upperbound])

		if np.sum(mask) >= 3:
			gmodel2 = GaussianModel()
			params2 = gmodel.make_params(amplitude=result.params["amplitude"], center=result.params["center"], sigma=result.params["sigma"])
			result2 = gmodel.fit(hist[mask], params, x=xbins[mask])
		else:
			result2 = result
			mask = np.full(len(xbins), True)
			print("WARNING: Unable to perform second fit for channel {}/{} of run '{}'".format(channelName, dataset, self.runpath))

		if verbose: print("Channel {} 1 p.e.\n\tvalue\t{} \n\tstd\t {}".format(channelName, result2.params["center"], result2.params["sigma"]))

		if plot:
			barplot = plt.bar(xbins, hist, width=xbins[1]-xbins[0], alpha=0.6, label="Ch {} ({:.4f})".format(channelName, result2.params["center"].value))
			plt.plot(xbins[mask], result2.best_fit, ".--", color=barplot.patches[0].get_fc()[:3])

			plt.xlabel("{} ({})".format(dataset, unit))

			plt.legend()

		return result2.params["center"].value, result2.params["center"].stderr
