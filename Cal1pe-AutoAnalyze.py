from Cal1pe import Cal1peAnalysis
import os

import numpy as np
import pandas as pd
import time

folder1pe = "Autoruns/AHCAL-2020-10-21-evening/"
# folder1pe = "1pe-Cal/BC408_fullRange/"
outfile = folder1pe + "processed.csv"
intgralMinValue = -300

runfolders = [dI for dI in os.listdir(folder1pe) if os.path.isdir(os.path.join(folder1pe, dI)) and dI.startswith("run") and dI.endswith("cal1pe-v4")]

channels = ["A", "C", "E", "G"]

datapoints = len(runfolders)
timeAxis = np.zeros(datapoints)
dateTime = []
fileName = []
val1pe = np.zeros((len(channels), datapoints))
err1pe = np.zeros((len(channels), datapoints))
valped = np.zeros((len(channels), datapoints))
errped = np.zeros((len(channels), datapoints))

for i, runfolder in enumerate(runfolders):
	print("Analyzing '{}'".format(runfolder))
	cal = Cal1peAnalysis(folder1pe + runfolder + "/", 450, 650)
	dateTime.append(cal.config["runDateTime"])
	fileName.append(runfolder)
	timeAxis[i] = cal.config["startTime"]
	for j, ch in enumerate(channels):
		cal.analyzeChannel(ch)
		val1pe[j, i], err1pe[j, i] = cal.generateHistAndFit(ch, "1pe", intgralMinValue, 200)
		valped[j, i], errped[j, i] = cal.generateHistAndFit(ch, "pedestal", None, 100)


# Convert to DataFrame
df = pd.DataFrame(data={
	"timeAxis": timeAxis,
	"dateTime": dateTime,
	"fileName": fileName,
	"valA": val1pe[0, :],
	"valC": val1pe[1, :],
	"valE": val1pe[2, :],
	"valG": val1pe[3, :],
	"errA": err1pe[0, :],
	"errC": err1pe[1, :],
	"errE": err1pe[2, :],
	"errG": err1pe[3, :],
	"valPedA": valped[0, :],
	"valPedC": valped[1, :],
	"valPedE": valped[2, :],
	"valPedG": valped[3, :],
	"errPedA": errped[0, :],
	"errPedC": errped[1, :],
	"errPedE": errped[2, :],
	"errPedG": errped[3, :]
	})

df.to_csv(outfile)

