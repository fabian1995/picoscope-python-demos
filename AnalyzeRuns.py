import ctypes
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import time
import scipy.signal as sig

from WaveformReader import Run, MainPeak

# Parameters
analysisThreshold = 500

# Initialize run
run5 = Run("testdata/run5/")

chA1 = run5.readBulk(0, "A", pedestal=900)
xaxis = run5.getXaxis()
signal = -chA1[0, :]

threshold_mV = - run5.config["threshold_mV"] + run5.config["analogueOffset"] * 1000

peaks, properties = sig.find_peaks(signal, height=15, prominence=10)

mp = MainPeak(peaks[0], properties["left_bases"][0], properties["right_bases"][0], xaxis, signal)

print(properties)

plt.plot(xaxis, signal)

for peak, lb, rb, c in zip(peaks, properties["left_bases"], properties["right_bases"], ["r", "g"]):
	plt.axvline(xaxis[peak], color=c, linewidth=0.75)
	plt.axvline(xaxis[lb], color=c, linewidth=0.75, linestyle=":")
	plt.axvline(xaxis[rb], color=c, linewidth=0.75, linestyle=":")
	break

plt.axhline(threshold_mV, color="cyan")

mp.fitAndIntersect(threshold_mV, plot=True)

plt.show()