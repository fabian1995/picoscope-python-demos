import ctypes
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import time

# Number of waveforms to be recorded
nWaveforms = 10000
nBulks = 1
maxsamples = 1500

filepath = "testdata/ovfl-test-3b/Bulk{}-Ch{}-wf{}-{}.bin"
ovflpath = "testdata/ovfl-test-3b/Bulk{}-Overflow-wf{}-{}.bin"

startTime = time.time()

# Read DataFrames
bufferType = ctypes.c_int16 * maxsamples
ovflType = ctypes.c_int16 * nWaveforms

averages = np.zeros(nWaveforms)
for bulk in range(nBulks):
	segFrom = 0
	segTo = nWaveforms-1
	
	readBufferA = np.zeros((nWaveforms, maxsamples), dtype=np.int16)

	with open(filepath.format(bulk, "A", segFrom, segTo), "rb") as file:
		for i in range(nWaveforms):
			content = file.read(ctypes.sizeof(bufferType))
			pointer = ctypes.cast(content, ctypes.POINTER(bufferType))
			readBufferA[i, :] = np.asarray(pointer.contents)

	readBufferC = np.zeros((nWaveforms, maxsamples), dtype=np.int16)

	with open(filepath.format(bulk, "C", segFrom, segTo), "rb") as file:
		for i in range(nWaveforms):
			content = file.read(ctypes.sizeof(bufferType))
			pointer = ctypes.cast(content, ctypes.POINTER(bufferType))
			readBufferC[i, :] = np.asarray(pointer.contents)

	
	readBufferE = np.zeros((nWaveforms, maxsamples), dtype=np.int16)

	with open(filepath.format(bulk, "E", segFrom, segTo), "rb") as file:
		for i in range(nWaveforms):
			content = file.read(ctypes.sizeof(bufferType))
			pointer = ctypes.cast(content, ctypes.POINTER(bufferType))
			readBufferE[i, :] = np.asarray(pointer.contents)

	readBufferG = np.zeros((nWaveforms, maxsamples), dtype=np.int16)

	with open(filepath.format(bulk, "G", segFrom, segTo), "rb") as file:
		for i in range(nWaveforms):
			content = file.read(ctypes.sizeof(bufferType))
			pointer = ctypes.cast(content, ctypes.POINTER(bufferType))
			readBufferG[i, :] = np.asarray(pointer.contents)

	readBufferOverflow = np.zeros(maxsamples, dtype=np.int16)

	with open(ovflpath.format(bulk, segFrom, segTo), "rb") as file:
		content = file.read(ctypes.sizeof(ovflType))
		pointer = ctypes.cast(content, ctypes.POINTER(ovflType))
		readBufferOverflow = np.asarray(pointer.contents)
		
	xmin = 0
	xmax = maxsamples
	xaxis = np.arange(xmin, xmax) 
	subplots = 10
	offset = 30
	if bulk < 3:
		plt.figure("Waveform {}".format(bulk))
		for i, j in zip(range(subplots), range(offset, offset+subplots)):
			plt.subplot(subplots, 1, i+1)
			plt.plot(xaxis, readBufferA[j, :], label="Ch A")
			plt.plot(xaxis, readBufferC[j, :], label="Ch C")
			plt.plot(xaxis, readBufferE[j, :], label="Ch E")
			plt.plot(xaxis, readBufferG[j, :], label="Ch G")
			plt.text(0.05, 0.25, " Channels: HGFEDCBA", transform=plt.gca().transAxes, fontsize=9)
			plt.text(0.05, 0.05, "Overflow: {:08b}".format(readBufferOverflow[j]), transform=plt.gca().transAxes)


print("Read time: {:.2f} s".format(time.time()-startTime))


plt.legend()

plt.show()
