import ctypes
import numpy as np
import time

nWaveforms = 10000
maxsamples = 3000

bufferAMax = []
bufferType = ctypes.c_int16 * maxsamples

print("Size of array: {}".format(ctypes.sizeof(bufferType)))

for i in range(nWaveforms):
	bufferAMax.append((maxsamples*ctypes.c_int16)())


bufferAMax[0][0] = 2
bufferAMax[1][2] = 1
bufferAMax[2][1] = -1

print("Content of array:")
print(np.asarray(bufferAMax))

startTime = time.time()

# parr = pa.array(bufferAMax, type=pa.int16())
# nparr = np.ctypeslib.as_array(bufferAMax)
# nparr = np.asarray(bufferAMax, dtype=np.int16)
with open("demo.pickle", "wb") as file:
	for i in range(nWaveforms):
		file.write(bufferAMax[i])

print("Save time: {:.2f} s".format(time.time()-startTime))

readBuffer = np.zeros((nWaveforms, maxsamples), dtype=np.int16)

startTime = time.time()

with open("demo.pickle", "rb") as file:
	for i in range(nWaveforms):
		content = file.read(ctypes.sizeof(bufferType))
		pointer = ctypes.cast(content, ctypes.POINTER(bufferType))
		readBuffer[i, :] = np.asarray(pointer.contents)

print("Read time: {:.2f} s".format(time.time()-startTime))

print(readBuffer)