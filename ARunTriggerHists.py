import ctypes
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import time
import scipy.signal as sig

from WaveformReader import Run, MainPeak

# Parameters
analysisThreshold = 500
fitWidth = 100

# Initialize run
run2 = Run("AHCAL/run2/")

channels = ["A", "C", "E", "G"]

nBulks = 5
trigVals = np.zeros((len(channels), nBulks*10000))
highest = np.zeros((len(channels), nBulks*10000))

for i, ch in enumerate(channels):
	for bulk in range(nBulks):
		print("Read bulk {} of ch {}".format(bulk, ch))

		chA1 = -run2.readBulk(bulk, ch, pedestal=900)
		xaxis = np.arange(chA1.shape[1])
		
		trigVals[i, bulk*10000:(bulk+1)*10000] = chA1[:,1000]
		highest[i, bulk*10000:(bulk+1)*10000] = np.max(chA1, axis=1)

print(trigVals)

# Plot histograms
plt.figure("Signal value at trigger")

for i, ch in enumerate(channels):
	data = trigVals[i, :]
	plt.hist(data[data < 5000], bins=300, label="Channel {}".format(ch), density=False, alpha=0.6)

plt.legend()

# Plot max peak height
plt.figure("Maximum peak height")

for i, ch in enumerate(channels):
	data = highest[i, :]
	plt.hist(data[data < 5000], bins=300, label="Channel {}".format(ch), density=False, alpha=0.6)

plt.legend()

# Show plot
plt.show()