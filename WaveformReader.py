import json
import ctypes
import numpy as np
import matplotlib.pyplot as plt
from PicoUtils import adc2mV

from lmfit.models import ExponentialModel, PowerLawModel, ExpressionModel
from lmfit import fit_report, Parameters

class Run:
	def __init__(self, runpath):
		self.runpath = runpath

		with open(runpath + "config.json") as json_file: 
			self.config = json.load(json_file)

		self.maxsamples = self.config["preTriggerSamples"] + self.config["postTriggerSamples"]
		self.bufferType = ctypes.c_int16 * self.maxsamples

	def readBulk(self, bulkNumber, channelName, pedestal=None):
		readBuffer = np.zeros((self.config["nWaveforms"], self.maxsamples), dtype=np.int16)
		filepath = self.runpath + "Bulk{}-Ch{}-wf{}-{}.bin"

		with open(filepath.format(bulkNumber, channelName, 0, self.config["nWaveforms"]-1), "rb") as file:
			for i in range(self.config["nWaveforms"]):
				content = file.read(ctypes.sizeof(self.bufferType))
				pointer = ctypes.cast(content, ctypes.POINTER(self.bufferType))
				readBuffer[i, :] = np.asarray(pointer.contents)

		data = adc2mV(readBuffer.astype('float64'), self.config["channelRange"], self.config["maxADC"])

		if pedestal is not None:
			pedestal = np.average(data[:, :pedestal], axis=1)
			data -= pedestal[:, None]
		
		return data

	def getXaxis(self):
		return np.arange(self.config["preTriggerSamples"] + self.config["postTriggerSamples"]) * self.config["timeIntervalns"]


	def x2index(self, x):
		return int(x/self.config["timeIntervalns"])


	def index2x(self, index):
		return index * self.config["timeIntervalns"]




class MainPeak:
	def __init__(self, center, lb, rb, height, xaxis, yaxis):
		self.center = center
		self.lb = lb
		self.rb = rb
		self.height = height
		self.xaxis = xaxis
		self.yaxis = yaxis


	def fitAndSubtract(self):
		# Convert coorinates to indices
		c = int(self.center)
		r = int(self.rb)

		# Right fit
		rmodel = ExpressionModel('off + ampl * exp(-(x-x0)/tau)')
		params = Parameters()
		params.add('off', value=0)
		params.add('ampl', value=self.height/2, min=0)
		params.add('x0', value=self.xaxis[self.center], min=self.xaxis[self.center]-100, max=self.xaxis[self.center]+100)
		params.add('tau', value=(self.xaxis[self.rb]-self.xaxis[self.center])/50, min=0)
		rresult = rmodel.fit(self.yaxis[c:r+1], params=params, x=self.xaxis[c:r+1])

		# Create new subtracted signal, set negative values to 0
		xnew = self.xaxis[c:]
		ynew = self.yaxis[c:]-rresult.eval(x=self.xaxis[c:])
		ynew[ynew < 0] = 0

		return xnew, ynew


	def fitAndIntersect(self, threshold, plot=False, do_fit=True):
		# Convert coorinates to indices
		l = int(self.lb)
		c = int(self.center)
		r = int(self.rb)

		# Left fit
		# lmodel = ExpressionModel('off + x * k')
		# params = lmodel.make_params(off=0, k = 1)#amp=1, x0=self.lb, tau=2.5)
		# lresult = lmodel.fit(self.yaxis[l:c+1], params=params, x=self.xaxis[l:c+1])

		# # plt.plot(self.xaxis[l:c], lmodel.eval(params, x=self.xaxis[l:c]))
		# if plot: plt.plot(self.xaxis[l:c+1], lresult.best_fit)
		# start = (threshold - lresult.params["off"].value) / lresult.params["k"].value

		# Right fit
		rmodel = ExpressionModel('off + ampl * exp(-(x-x0)/tau)')
		# params = rmodel.make_params(off=0, ampl=self.height/2, x0=self.xaxis[self.center], tau=(self.xaxis[self.rb]-self.xaxis[self.center])/10)
		params = Parameters()
		params.add('off', value=0)
		params.add('ampl', value=self.height/2, min=0)
		params.add('x0', value=self.xaxis[self.center], min=self.xaxis[self.center]-100, max=self.xaxis[self.center]+100)
		params.add('tau', value=(self.xaxis[self.rb]-self.xaxis[self.center])/50, min=0) #

		if not do_fit:
			plt.plot(self.xaxis[c:r], rmodel.eval(params, x=self.xaxis[c:r]))
			return 0, 0

		rresult = rmodel.fit(self.yaxis[c:r+1], params=params, x=self.xaxis[c:r+1])

		print(rresult.fit_report())

		if plot: plt.plot(self.xaxis[c:r+1], rresult.best_fit)
		p = rresult.params
		end = p["x0"].value - p["tau"].value * np.log((threshold-p["off"].value) / p["ampl"].value)

		# if plot:
		# 	plt.axvline(start)
		# 	plt.axvline(end)

		plt.figure("Subtracted Signal")
		xnew = self.xaxis[c:]
		ynew = self.yaxis[c:]-rresult.eval(x=self.xaxis[c:])
		# plt.plot(self.xaxis[c:], self.yaxis[c:]-rresult.eval(x=self.xaxis[c:]))

		return xnew, ynew



